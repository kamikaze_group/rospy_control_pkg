#! /usr/bin/env python
import rospy
import sys
import math
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
import numpy as np
from sensor_msgs.msg import Imu
from std_msgs.msg import String
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import mav_msgs.msg as mav_msgs
from pid import PID
from control import Control
from std_msgs.msg import Float64
import parameters as p
from pynput import keyboard

alt_set_point   = 5
roll_set_point  = 0
pitch_set_point = 0
yaw_set_point   = 0
position_z      = 0
roll            = 0
pitch           = 0
yaw             = 0
prev_nsec       = 0
nsec_diff       = 0.0
cont            = 0

def odom_callback(msg):
    global roll, pitch, yaw 
    global position_z
    global imu_rate
    global prev_nsec
    global nsec_diff
    position_z = msg.pose.pose.position.z
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
    roll1 = roll  # 180 / 3.14
    pitch1 = pitch # 180/3.14
    pitch = roll1
    roll = pitch1  
    yaw = yaw # 180/3.14    
    nsec_diff = (float(msg.header.stamp.to_nsec())/1000000000) - prev_nsec
    prev_nsec = float(msg.header.stamp.to_nsec())/1000000000 
    # print (prev_nsec)
    # if (not(nsec_diff == 0.01)):
    #     print("Error")
    imu_rate.sleep()



def on_press(key):
    global alt_set_point   
    global roll_set_point  
    global pitch_set_point 
    global yaw_set_point 

    if (key ==  keyboard.KeyCode.from_char('w')):
        print("Es una W!")
        alt_set_point+= 1
    elif (key ==  keyboard.KeyCode.from_char('s')):
        alt_set_point-= 1
    elif (key ==  keyboard.KeyCode.from_char('a')):
        roll_set_point = 0.1
    elif (key ==  keyboard.KeyCode.from_char('d')):
        roll_set_point = -0.1
    elif(key ==  keyboard.KeyCode.from_char('i')):
        pitch_set_point = 0.1
    elif(key ==  keyboard.KeyCode.from_char('k')):
        pitch_set_point = -0.1
    elif(key ==  keyboard.KeyCode.from_char('l')):
        yaw_set_point += 0.1
    elif(key ==  keyboard.KeyCode.from_char('j')):
        yaw_set_point -= 0.1
    print("\nroll:\t{}, pitch:\t{}, yaw:\t{}, alti:\t{}".format(roll_set_point,pitch_set_point,yaw_set_point,alt_set_point))

def on_release(key):
    global alt_set_point   
    global roll_set_point  
    global pitch_set_point 
    global yaw_set_point 

    if (key ==  keyboard.KeyCode.from_char('a')) or (key ==  keyboard.KeyCode.from_char('d')):
        roll_set_point = 0
    elif (key ==  keyboard.KeyCode.from_char('i')) or (key ==  keyboard.KeyCode.from_char('k')):
        pitch_set_point =0
    print("\nroll:\t{}, pitch:\t{}, yaw:\t{}, alti:\t{}".format(roll_set_point,pitch_set_point,yaw_set_point,alt_set_point))

#################################################
###### ------------ Main ----------------- ######
#################################################

rospy.init_node('topic_subscriber') # the original name sphero might be the same as other node.

#rates
imu_rate     = rospy.Rate(100)  #20HZ = 50ms, q es lo que usabamos en el drone
control_rate = rospy.Rate(100)

listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()

print("listener running")
sub_odom = rospy.Subscriber('/kamikaze/ground_truth/odometry', Odometry, odom_callback) # the original name odom might be the same as other function.

pub_motors = rospy.Publisher('/kamikaze/command/motor_speed' , mav_msgs.Actuators, queue_size=10)
pub_servo  = rospy.Publisher('/kamikaze/command/elevon_angle', mav_msgs.Actuators, queue_size=10)

#---------------------------------------------------------------------
#plotters z
plot_altitude_z = rospy.Publisher("/plot_altitude_z", Float64, queue_size=10)
plot_error_z = rospy.Publisher("/plot_error_z", Float64, queue_size=10)
plot_sp_z = rospy.Publisher("/plot_sp_z", Float64, queue_size=10)
plot_pid_z = rospy.Publisher("/plot_pid_z", Float64, queue_size=10)
#---------------------------------------------------------------------
#plotters roll
plot_roll = rospy.Publisher("/plot_roll", Float64, queue_size=10)
plot_error_roll = rospy.Publisher("/plot_error_roll", Float64, queue_size=10)
plot_sp_roll = rospy.Publisher("/plot_sp_roll", Float64, queue_size=10)
plot_pid_roll = rospy.Publisher("/plot_pid_roll", Float64, queue_size=10)
#---------------------------------------------------------------------
#plotters pitch
plot_pitch = rospy.Publisher("/plot_pitch", Float64, queue_size=10)
plot_error_pitch = rospy.Publisher("/plot_error_pitch", Float64, queue_size=10)
plot_sp_pitch = rospy.Publisher("/plot_sp_pitch", Float64, queue_size=10)
plot_pid_pitch = rospy.Publisher("/plot_pid_pitch", Float64, queue_size=10)
#---------------------------------------------------------------------
#plotters roll
plot_yaw = rospy.Publisher("/plot_yaw", Float64, queue_size=10)
plot_error_yaw = rospy.Publisher("/plot_error_yaw", Float64, queue_size=10)
plot_sp_yaw = rospy.Publisher("/plot_sp_yaw", Float64, queue_size=10)
plot_pid_yaw = rospy.Publisher("/plot_pid_yaw", Float64, queue_size=10)
#---------------------------------------------------------------------
#plotters motores
plot_mot1 = rospy.Publisher("/plot_mot1", Float64, queue_size=10)
plot_mot2 = rospy.Publisher("/plot_mot2", Float64, queue_size=10)
plot_servo1 = rospy.Publisher("/plot_servo1", Float64, queue_size=10)
plot_servo2 = rospy.Publisher("/plot_servo2", Float64, queue_size=10)
#----------------------------------------------------------------------
motors = mav_msgs.Actuators()
servo = mav_msgs.Actuators()

#---------------------------------------------------------------------
leftMotor   =  p.M0
rightMotor  =  p.M0
leftServo   = -p.S0
rightServo  =  p.S0


altitude_pid = PID(p.altitude['kp'], p.altitude['ki'], p.altitude['kd'], 1    , -1)
roll_pid     = PID(p.roll['kp']    , p.roll['ki']    , p.roll['kd']    , 0.08 , -0.08)
pitch_pid    = PID(p.pitch['kp']   , p.pitch['ki']   , p.pitch['kd']   , 0.08 , -0.08)
yaw_pid      = PID(p.yaw['kp']     , p.yaw['ki']     , p.yaw['kd']     , 0.05 , -0.05)

altitude_control = Control(alt_set_point   ,0 ,0 , p.altitude['k'])
roll_control     = Control(roll_set_point  ,0 ,0 , p.roll['k'])
pitch_control    = Control(pitch_set_point ,0 ,0 , p.pitch['k'])
yaw_control      = Control(yaw_set_point   ,0 ,0 , p.yaw['k'])

old_roll = 0
old_alt = 0

servo.angular_velocities  = [leftServo,rightServo]
pub_servo.publish(servo)

while not rospy.is_shutdown():
    
    altitude_control._set_point = alt_set_point
    roll_control._set_point = roll_set_point
    pitch_control._set_point = pitch_set_point
    yaw_control._set_point= yaw_set_point

    altitude_control._error = altitude_control._set_point - position_z
    roll_control._error     = roll_control._set_point - roll
    pitch_control._error    = pitch_control._set_point - pitch
    yaw_control._error      = yaw_control._set_point - yaw

    # print (nsec_diff)
    altitude_control._output = altitude_pid.update_PID(altitude_control._error,nsec_diff)
    roll_control._output     = roll_pid.update_PID(roll_control._error,nsec_diff)
    pitch_control._output    = pitch_pid.update_PID(pitch_control._error,nsec_diff)
    yaw_control._output      = yaw_pid.update_PID(yaw_control._error,nsec_diff)

    altitude_control._output = np.ma.average([old_alt,altitude_control._output])
    old_alt = altitude_control._output
    roll_control._output = np.ma.average([old_roll,roll_control._output])
    old_roll = roll_control._output

    leftMotor  = p.M0  + ((altitude_control._output * altitude_control._k) + (roll_control._output * roll_control._k)) * p.k_ml
    rightMotor = p.M0  + ((altitude_control._output * altitude_control._k) - (roll_control._output * roll_control._k)) * p.k_mr
    #leftServo  = p.S0  + ((pitch_control._output * pitch_control._k) - (yaw_control._output * yaw_control._k)) * p.k_sl
    #rightServo = p.S0  + ((pitch_control._output * pitch_control._k) + (yaw_control._output * yaw_control._k)) * p.k_sr
    leftServo  = p.S0  -  p.k_sl * (yaw_control._output * yaw_control._k) + p.k_sl * (pitch_control._output * pitch_control._k)
    rightServo = p.S0  +  p.k_sr * (yaw_control._output * yaw_control._k) + p.k_sr * (pitch_control._output * pitch_control._k)

    leftMotor  = max(min(leftMotor,p.M0_max)  , p.M0_min)
    rightMotor = max(min(rightMotor,p.M0_max) , p.M0_min)

    leftServo  = max(min(leftServo,p.S0_max)  , p.S0_min)
    rightServo = max(min(rightServo,p.S0_max) , p.S0_min)
    
    #---------------------------------------------------------------------
    #plotters altura
    plot_altitude_z.publish(position_z)
    plot_error_z.publish(altitude_control._error)
    plot_sp_z.publish(altitude_control._set_point)
    plot_pid_z.publish(altitude_control._output * altitude_control._k)
    #---------------------------------------------------------------------
    #plotters roll
    plot_roll.publish(roll)
    plot_error_roll.publish(roll_control._error)
    plot_sp_roll.publish(roll_control._set_point)
    plot_pid_roll.publish(roll_control._output * roll_control._k)
    #---------------------------------------------------------------------
    #plotters pitch
    plot_pitch.publish(pitch)
    plot_error_pitch.publish(pitch_pid.d_term)
    plot_sp_pitch.publish(pitch_control._set_point)
    plot_pid_pitch.publish(pitch_control._output * pitch_control._k)
    #---------------------------------------------------------------------
    #plotters yaw
    plot_yaw.publish(yaw)
    plot_error_yaw.publish(yaw_control._error)
    plot_sp_yaw.publish(yaw_control._set_point)
    plot_pid_yaw.publish(yaw_control._output * yaw_control._k)
    #---------------------------------------------------------------------
    #plotters Motores
    plot_mot1.publish(leftMotor)
    plot_mot2.publish(rightMotor)
    plot_servo1.publish(leftServo)
    plot_servo2.publish(rightServo)
    #---------------------------------------------------------------------
    # print "%f %f %f\t|%f %f %f\t|\t%f %f\t|\t%f %f %f %f" %(altitude_control._set_point , position_z , altitude_control._output,
    #                                             roll_control._set_point , roll , roll_control._output ,
    #                                             altitude_control._output , roll_control._output, leftMotor 
    #                                             , rightMotor , leftServo , rightServo)
    # print "%f %f %f\t|\t%f %f %f %f\t|\t%f %f %f %f" %(altitude_control._set_point , position_z , altitude_control._error,
    #                                             altitude_control._output , roll_control._output , 
    #                                             pitch_control._output , yaw_control._output , leftMotor 
    #                                             , rightMotor,leftServo , rightServo)
    # print "%f %f %f\t|\t%f %f %f %f\t|\t%f %f %f %f" %(roll_control._set_point , roll , roll_control._error ,
    #                                             altitude_control._output , roll_control._output , 
    #                                             pitch_control._output , yaw_control._output , leftMotor 
    #                                             , rightMotor , leftServo , rightServo)

    # print "%f %f %f\t|\t%f %f %f %f\t|\t%f %f %f %f" %(pitch_control._set_point , pitch , pitch_control._error ,
    #                                             altitude_control._output , roll_control._output , 
    #                                             pitch_control._output , yaw_control._output , leftMotor 
    #                                             , rightMotor , leftServo , rightServo)

    # print "%f %f %f\t|\t%f %f %f %f\t|\t%f %f %f %f" %(yaw_control._set_point , yaw , yaw_control._error ,
    #                                             altitude_control._output , roll_control._output , 
    #                                             pitch_control._output , yaw_control._output , leftMotor 
    #                                             , rightMotor , leftServo , rightServo)
    motors.angular_velocities = [leftMotor,rightMotor]
    servo.angular_velocities  = [leftServo,rightServo]
    pub_motors.publish(motors)
    pub_servo.publish(servo)
    control_rate.sleep()

