import rospy
import sys
import math
import time
class Control(object):
    """ A basic control class.

    """

    def __init__(self, set_point, error, output,k):
        
        self.set_gains(set_point, error, output,k)
        # self.reset()

    def reset(self):
        """  Reset the state of this PID controller """
        self._set_point = 0.0 
        self._error = 0.0   
        self._output = 0.0  
        self._k = 1
        
    def set_gains(self, set_point, error, output,k): 
        
        self._set_point = set_point
        self._error = error
        self._output = output
        self._k = k
        
    @property
    def set_point(self):
        return self._set_point

    @property
    def error(self):
        return self._error

    @property
    def output(self):
        return self._output
    
    @property
    def k(self):
        return self._k


    def __str__(self):
        """ String representation of the current state of the controller. """
        result = ""
        result += "set_point:  " + str(self.set_point) + "\n"
        result += "error:  " + str(self.error) + "\n"
        result += "output:  " + str(self.output) + "\n"
        result += "k:  " + str(self.k) + "\n"
        return result
        
    