#! /usr/bin/env python

position_z = 0
roll = 0
pitch = 0.3
yaw = 0

#constantes
M0 = 980
M0_min = 750
M0_max = 1050

S0 = -0.1703   
S0_min = -1#-0.166666667 * 2
S0_max = 1#0.166666667  * 2

k_ml = 1
k_mr = 1
k_sl = 0.1
k_sr = 0.1

altitude={   
    'k' :0.8,
    'kp':120,
    'ki':1,
    'kd':65,
    }

roll={   
    'k' :0.5,
    'kp':190,
    'ki':2,
    'kd':40,
    }

pitch={   
    'k' :1.2,
    'kp':8.5,
    'ki':0.5,
    'kd':1,
    }

yaw={   
    'k' :0.8,
    'kp':9.2,
    'ki':0.5,
    'kd':2,
    }